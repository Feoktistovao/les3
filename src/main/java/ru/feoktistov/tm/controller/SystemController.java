package ru.feoktistov.tm.controller;

import ru.feoktistov.tm.service.DomainService;

public class SystemController {

    private DomainService domainService;

    public SystemController(DomainService domainService) {
        this.domainService = domainService;
    }

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int exportXML() throws Exception{
        System.out.println("[DATA EXPORT XML]");
        domainService.exportXML();
        System.out.println("[OK]");
        return 0;
    }
    public int exportJSON() throws Exception{
        System.out.println("[DATA EXPORT JSON]");
        domainService.exportJSON();
        System.out.println("[OK]");
        return 0;
    }
    public int exportBASE64() throws Exception{
        System.out.println("[DATA EXPORT BASE64]");
        domainService.exportBASE64();
        System.out.println("[OK]");
        return 0;
    }
    public int importXML() throws Exception{
        System.out.println("[DATA IMPORT XML]");
        domainService.importXML();
        System.out.println("[OK]");
        return 0;
    }
    public int importJSON() throws Exception{
        System.out.println("[DATA IMPORT JSON]");
        domainService.importJSON();
        System.out.println("[OK]");
        return 0;
    }
    public int importBASE64(){
        System.out.println("[DATA IMPORT BASE64]");
        domainService.importBASE64();
        System.out.println("[OK]");
        return 0;
    }

    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Denis Volnenko");
        System.out.println("denis@feoktistov.ru");
        return 0;
    }

}
