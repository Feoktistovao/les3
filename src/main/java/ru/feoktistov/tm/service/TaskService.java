package ru.feoktistov.tm.service;

import ru.feoktistov.tm.entity.Task;
import ru.feoktistov.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(Task task) {
        if (task ==null) return null;
        return taskRepository.create(task);
    }

    public void create(Collection<Task> tasks) {
        if (tasks==null || tasks.isEmpty())  return;
        taskRepository.create(tasks);
    }

    public Task create(String name) {
        return taskRepository.create(name);
    }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }

    public Task removeById(Long id) {
        return taskRepository.removeById(id);
    }

    public Task removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

}
