package ru.feoktistov.tm.constant;

public class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String DATA_IMPORT_JSON = "data-import-json";
    public static final String DATA_IMPORT_XML = "data-import-xml";
    public static final String DATA_IMPORT_BASE64 = "data-import-BASE64";
    public static final String DATA_EXPORT_BASE64 = "data-export-BASE64";
    public static final String DATA_EXPORT_JSON = "data-export-json";
    public static final String DATA_EXPORT_XML = "data-export-xml";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

}
